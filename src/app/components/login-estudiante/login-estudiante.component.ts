import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PerfilSociodemograficoService } from 'src/app/services/perfil-sociodemografico.service';

@Component({
  selector: 'app-login-estudiante',
  templateUrl: './login-estudiante.component.html',
  styleUrls: ['./login-estudiante.component.css']
})
export class LoginEstudianteComponent {

  constructor( public perfilSociodemagraficoService:PerfilSociodemograficoService ) { }

  loginEstudiate( validar:NgForm ){
    console.log(validar)
  }

}
