import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NavigationComponent } from './navigation/navigation.component';
import { EstudianteComponent } from 'src/app/components/estudiante/estudiante.component';
import { FuncionarioComponent } from 'src/app/components/funcionario/funcionario.component';
import { MainPageComponent } from 'src/app/components/main-page/main-page.component';
import { LoginEstudianteComponent } from 'src/app/components/login-estudiante/login-estudiante.component';
import { LoginFuncionarioComponent } from 'src/app/components/login-funcionario/login-funcionario.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    NavigationComponent,
    LoginEstudianteComponent,
    LoginFuncionarioComponent,
    EstudianteComponent,
    FuncionarioComponent,
    MainPageComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  exports:[
    NavigationComponent,
    LoginEstudianteComponent,
    LoginFuncionarioComponent,
    EstudianteComponent,
    FuncionarioComponent,
    MainPageComponent,
    FooterComponent
  ]
})
export class PerfilSociodemograficoModule { }
