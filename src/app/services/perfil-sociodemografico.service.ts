import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PerfilSociodemograficoService {

  private apiUrl:string = 'http://localhost:3000/api'

  constructor( private http:HttpClient ) { }

  get_estudiantes(){
    const url = `${this.apiUrl}/estudiante`
    return this.http.get(url)
  }

  get_estudiante(value:string){
    const url = `${this.apiUrl}/estudiante/${value}`
    const params:HttpParams = new HttpParams()
      .set("access_key","46771d9824ce268a93e76ac38c984638")
      .set("fields",'name;capital;callingCodes;region;alpha2Code');
    return this.http.get(url, {params})
  }
}
